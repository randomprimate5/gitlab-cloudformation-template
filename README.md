# GitLab HA CloudFormation Template

This is not a production template.

Main objective of this script is to debug common HA issues in AWS. 

## Create a Stack

* Store the template from this project in a file
* Visit your AWS Web Console and choose CloudFormation under Management Tools.
* Choose Create Stack
* Upload to S3 bucket and choose the stored template file

## Configure app servers

* SSH into each server and change the IP to the new allocated IPs
* Mount EFS
* Change the `extenral_url`, change the db to point to RDS new host and the Redis host to ElastiCache.
* Reconfigure
* Restart if necessary 

## Known Issues

* Security groups are too open.
* App servers require mounting of EFS. Mount with `sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 <efs-endpoint>:/ /efs`
* This version of CloudFormation requires re association of Security Groups for
RDS.
* Elastic IPs are used which are allocated on a best effort basis (i.e. you will not get the same one). Make sure to edit the `external_url` with the new ip.
* Second server might not have GitLab configured.
* EFS resource might need to be created and `/efs` directory on the second server as well.